package com.examsoft.app.LegacyTest;

import com.examsoft.app.Legacy.base.base;
import com.examsoft.app.Legacy.flows.ExamNowFlow;
import com.examsoft.app.Legacy.flows.Login;
import com.examsoft.app.Legacy.pages.AppsPage;
import com.examsoft.app.Legacy.pages.AssessmentPage;
import com.examsoft.app.Legacy.pages.ExamNowPage;
import com.examsoft.app.Legacy.pages.LandingPage;
import com.examsoft.app.Legacy.flows.Assessments;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ETELocalLoginTestLeg extends base {

    //WebElement adminLogin =

    @Test
    public void fullLocalTest() throws IOException, InterruptedException {


        driver = initializeDriver();
        driver2 = initializeStdDriver();

        Login adminLogin = new Login(driver);
        Login stuLogin = new Login(driver2);
        LandingPage nav = new LandingPage(driver);
        Assessments assessments = new Assessments(driver);
        ExamNowFlow examNowFlow = new ExamNowFlow(driver,driver2);


        driver.get("https://orange.examsoft.com/GKWeb/login/QAone");
        driver2.get("https://xnw002-ui-qa.examsoft.com/login");

        //admin login & exam creation
        adminLogin.adminLocalLogin();
        nav.getNavAssessmentBtn().click();
        assessments.createARExam();

        //post exam; record posting ID
        String pID = assessments.postARExam();

        //navigate to examNow, login, filter for exam, start (admin)
        nav.getNavExamNowBtn().click();
        adminLogin.adminExamNowLogin();
        examNowFlow.administerExam(pID);

        //student login & and answering questions
        stuLogin.studentExamNowLogin();

        examNowFlow.takeExam();


        driver.quit();
        driver2.quit();

        driver = null;
        driver2 = null;



    }

    public void loginCheck() throws IOException, InterruptedException {
        driver2 = initializeStdDriver();
        driver2.get("https://xnw002-ui-qa.examsoft.com/login");
        Login stuLogin = new Login(driver2);

        stuLogin.studentExamNowLogin();

        //Thread.sleep(10000);
        //WebElement selections = driver2.findElement(By.xpath("/html/body/app-layout/div/main/div/poll-question/div/div/form/div/div/app-question/div/form/es-mc-choices//ol"));
        //List<WebElement> li =selections.findElements(By.tagName("li"));

        WebElement shadowroot1 = expandRootElement(driver2.findElement(By.xpath("//es-mc-choices[@class='hydrated']")));
        List <WebElement> answers = shadowroot1.findElements(By.tagName("li"));

        waitForElementAndClick(answers.get(2));
    }
}
