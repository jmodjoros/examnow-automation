package com.examsoft.app.Enterprise;

import com.examsoft.app.Enterprise.base.base;
import com.examsoft.app.Enterprise.flows.AdminFlows;
import com.examsoft.app.Enterprise.flows.AssessmentFlows;
import com.examsoft.app.Enterprise.flows.ExamNowFlow;
import com.examsoft.app.Enterprise.flows.LoginFlows;

import org.openqa.selenium.Point;
import org.testng.annotations.Test;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.io.IOException;


public class ETELocalLoginTestPhx extends base {
    public final Logger log = LogManager.getLogger(ETELocalLoginTestPhx.class);

    //public static Logger log = LogManager.getLogger(base.class.getName());
    @Test
    public void fullLocalTest() throws IOException, InterruptedException {

        driver2 = initializeStdDriver();
        log.info("Student driver is initialized");

        driver = initializeDriver();
        log.info("Admin driver is initialized");

        driver.manage().window().setPosition(new Point(0,200));
        //driver2.manage().window().setPosition(new Point(200,0));

        LoginFlows adminlogin = new LoginFlows(driver);
        LoginFlows stulogin = new LoginFlows(driver2);
        AssessmentFlows assessmentFlows = new AssessmentFlows(driver);
        AdminFlows adminFlows = new AdminFlows(driver);
        ExamNowFlow examNowFlows = new ExamNowFlow(driver,driver2);

        String assessmentTitle;
        driver.get("https://phx002-ui-qa.examsoft.com/login");
        log.info("Admin driver navigates to phx002 login");

        driver2.get("https://xnw002-ui-qa.examsoft.com/login");
        log.info("Student driver navigates to xnw002 login");



        adminlogin.LocalAdminLogin();
        assessmentFlows.navToAssessments();
        assessmentTitle = assessmentFlows.createAssessment();
        assessmentFlows.postAssessment();
        adminFlows.navToExamNow();
        adminlogin.adminExamNowLogin();
        examNowFlows.administerExam(assessmentTitle);

        stulogin.studentExamNowLogin();
        examNowFlows.takeExam();


        driver.quit();
        driver2.quit();

        driver = null;
        driver2 = null;


    }
}
