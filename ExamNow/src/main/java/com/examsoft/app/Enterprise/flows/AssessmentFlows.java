package com.examsoft.app.Enterprise.flows;

import com.examsoft.app.Enterprise.base.base;
import com.examsoft.app.Enterprise.pages.AssessmentPage;
import com.examsoft.app.Enterprise.pages.LandingPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class AssessmentFlows extends base {

    public AssessmentFlows(WebDriver driver)  {
        this.driver = driver;
    }

    public void navToAssessments() throws InterruptedException {
        LandingPage l = new LandingPage(driver);
        l.getDeptListBtn().click();
        l.getDepartment().click();
        l.getCourse().click();
        Thread.sleep(100);
        Assert.assertEquals(driver.getTitle(), "Examsoft | Assessment List");
    }

    public String createAssessment() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        SimpleDateFormat todaysDate = new SimpleDateFormat("MM.dd.yyyy.HH.mm.ss");
        Date date = new Date();

        String assessmentTitle = ("AutoENowTest"+todaysDate.format(date));
        AssessmentPage a = new AssessmentPage(driver);


        a.getCreateAssessment().click();

        a.getAssessmentTitle().sendKeys(assessmentTitle);

        Select examType = new Select(a.getSelectExamType());
        examType.selectByVisibleText("Audience Response");

        Thread.sleep(1000);
        a.getNextButton().click();

        Thread.sleep(2000);

        Assert.assertEquals(driver.getTitle(),"Examsoft | Assessment Builder");
        a.getSlideoutButton().click();
        a.getFindQuestions().click();

        Thread.sleep(2000);

        a.getSelectAllQs().click();
        a.getSaveButton().click();

        Thread.sleep(1000);

        return assessmentTitle;

    }

    public void postAssessment() throws InterruptedException {
        AssessmentPage postingWindow = new AssessmentPage(driver);
        SimpleDateFormat todaysDate = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();

        postingWindow.getSaveContinue().click();
        postingWindow.getAssignStudentsButton().click();

        Thread.sleep(1000);

        postingWindow.getSelectAllQs().click();
        closeModalIfPresent();
        postingWindow.getSaveStudent().click();
        postingWindow.getScheduledOnField().sendKeys(todaysDate.format(date)+" 11:59 pm");
        closeModalIfPresent();
        postingWindow.getFinalizeBtn().click();
        postingWindow.getPostBtn().click();
    }

}
