package com.examsoft.app.Enterprise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
    WebDriver driver;

    By department = By.xpath("//a[contains(text(),'AutoDPT')]");
    By course = By.xpath("//grid-cell[@class='truncated-container ng-scope ng-isolate-scope']//a[@class='ng-binding'][contains(text(),'Automation ExamNow')]");
    By departmentListButton = By.xpath("//a[@class='right ng-scope']");
    By adminDropdown = By.xpath("//div[@class='admin-menu__button']");
    By audienceResponseNav = By.xpath("//a[contains(text(),'Audience Response')]");

    public LandingPage(WebDriver driver) {
        this.driver = driver;
    }
    public WebElement getAudienceResponseBtn(){
        return driver.findElement(audienceResponseNav);
    }
    public WebElement getAdminDropdown(){
        return driver.findElement(adminDropdown);
    }
    public WebElement getDeptListBtn(){
        return driver.findElement(departmentListButton);
    }

    public WebElement getCourse(){
        return driver.findElement(course);
    }

    public WebElement getDepartment(){
        return driver.findElement(department);
    }
}
