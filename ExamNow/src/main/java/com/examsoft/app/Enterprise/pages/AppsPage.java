package com.examsoft.app.Enterprise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AppsPage {
  WebDriver driver;

    By loginExamNow = By.cssSelector("img[alt='Login to ExamNow']");
    By loginExamSoft = By.cssSelector("img[alt='Login to ExamSoft']");
    By selectInstitution = By.xpath("//*[@id='institution']/div/input");
    By continueBtn = By.xpath("//button[@class='progressive']");
    By usernameField = By.id("username");
    By passwordField = By.id("password");
    By submitBtn = By.xpath("//button[@class='action']");
    By iAgreeChkBx = By.xpath("//label[contains(text(),'I agree to the ExamNow Terms and Conditions')]");

    public AppsPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getAgreeChkBx(){
        return driver.findElement(iAgreeChkBx);
    }
    public WebElement getLoginExamNow() {
        return driver.findElement(loginExamNow);
    }


    public WebElement getSelectInstitution() {
        return driver.findElement(selectInstitution);
    }

    public WebElement getContinueBtn() {
        return driver.findElement(continueBtn);
    }

    public WebElement getUsernameField(){
        return driver.findElement(usernameField);
    }

    public WebElement getPasswordField(){
        return driver.findElement(passwordField);
    }

    public WebElement getSubmitBtn(){
        return driver.findElement(submitBtn);
    }
/*
    public void SAMLLogin() throws IOException {
        //selenium code
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Automation/src/credentials.properties");

        prop.load(fis);
        //Create Driver object for browser of choice
        driver = initializeDriver();
        driver.get("https://www.google.com");
        WebDriverWait wait = new WebDriverWait(driver,30);

        driver.get("https://apps-qa.examsoft.com/");
        System.out.println("Title of page: "+driver.getTitle());
        System.out.println("Validate that URL is correct: "+driver.getCurrentUrl());
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("#institution > div > input[type=text]")).sendKeys("QATHREE - Dev");
        driver.findElement(By.xpath("//*[@id='institution']/div/div/div")).click();
        System.out.println("Selected QATHREE");
        driver.findElement(By.className("progressive")).click();

        System.out.println("Launched ExamNow page");
        driver.findElement(By.xpath("/html/body/app-root/main/div/app-home/section[1]/div/es-panel[2]/a/button")).click();
        driver.findElement(By.cssSelector("#institution > div > input[type=text]")).sendKeys("QATHREE - Dev");
        driver.findElement(By.xpath("//*[@id='institution']/div/div/div")).click();//System.out.println(driver.getPageSource());
        System.out.println("Selected QATHREE on ExamNow page");
        driver.findElement(By.className("progressive")).click();

        System.out.println("Launched IDP");
        driver.findElement(By.name("loginfmt")).sendKeys(prop.getProperty("ssoAdminUsername"));
        driver.findElement(By.id("idSIButton9")).click();
        driver.findElement(By.name("passwd")).sendKeys("examsoft1!");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        //Current pw: ID: currentPassword
        //new pw: ID: newPassword
        //confirm password: ID: confirmNewPassword

        WebElement signInButton1;
        signInButton1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]")));
        signInButton1.click();

        WebElement signInButton2;
        signInButton2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("idSIButton9")));
        signInButton2.click();

        driver.findElement(By.cssSelector("div.app-layout-wrapper div.main-inner eula:nth-child(2) es-agreement.hydrated div.es-agreement div.agreement-control:nth-child(2) > label:nth-child(2)")).click();
        driver.findElement(By.className("progressive")).click();

        driver.close();
        //driver.quit();
    }*/
}
