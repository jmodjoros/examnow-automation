package com.examsoft.app.Enterprise.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.*;

public class base {
    public WebDriver driver;
    public WebDriver driver2;

    Properties prop = new Properties();

    public WebDriver initializeDriver() throws IOException {
        //WebDriverManager.getInstance(CHROME).setup();


        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Auto/ExamNow/credentials.properties");
        prop.load(fis);
        String browserName = prop.getProperty("browser");


        //chrome
        if (browserName.equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            //System.setProperty("webdriver.chrome.driver", "/Users/joshrobbins/Documents/Dev Resources/chromedriver");
            driver = new ChromeDriver();
        }
        //firefox
        else if (browserName.equals("firefox")) {
            System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }

    public WebDriver initializeStdDriver() throws IOException {


        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Auto/ExamNow/credentials.properties");
        prop.load(fis);
        String browserName = prop.getProperty("browser");


        //chrome
        if (browserName.equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            //System.setProperty("webdriver.chrome.driver", "/Users/joshrobbins/Documents/Dev Resources/chromedriver");
            driver2 = new ChromeDriver();
        }
        //firefox
        else if (browserName.equals("firefox")) {
            WebDriverManager.firefoxdriver().setup();
            //System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
            driver2 = new FirefoxDriver();
        }

        driver2.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver2;
    }

    public void closeModalIfPresent() {
        try {
            WebElement successToast =
                    new WebDriverWait(driver, 5)
                            .ignoring(StaleElementReferenceException.class)
                            .until(ExpectedConditions.elementToBeClickable(By.cssSelector(".toast-success")));
            successToast.click();
        } catch (TimeoutException | StaleElementReferenceException error) {
            // if element is not present, nothing to be done
        }
    }

    public WebElement expandRootElement(WebElement element) {
        WebElement ele = (WebElement) ((JavascriptExecutor) driver2).executeScript("return arguments[0].shadowRoot",element);
        return ele;
    }

    public void waitForElementAndClickStu(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver2, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }
    public void waitForElementAndClick(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }
}
