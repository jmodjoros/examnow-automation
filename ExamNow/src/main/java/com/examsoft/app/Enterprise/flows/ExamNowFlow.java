package com.examsoft.app.Enterprise.flows;

import com.examsoft.app.Enterprise.base.base;
import com.examsoft.app.Enterprise.pages.AudienceResponsePage;
import com.examsoft.app.Enterprise.pages.ExamNowPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

public class ExamNowFlow extends base {
    public ExamNowFlow(WebDriver driver, WebDriver driver2)  {
        this.driver = driver;
        this.driver2 = driver2;



    }
    public void administerExam(String title) throws InterruptedException, IOException {

        ExamNowPage e = new ExamNowPage(driver);
        Thread.sleep(2000);

        WebElement nameFilter = e.getNameFilter();

        Thread.sleep(2000);
        nameFilter.sendKeys(title);
        WebElement startBtn = e.getStartBtn();

        waitForElementAndClick(startBtn);

    }
    public void takeExam() throws InterruptedException {
        AudienceResponsePage takeExam = new AudienceResponsePage(driver2);
        AudienceResponsePage administerExam = new AudienceResponsePage(driver);

        Thread.sleep(5000);
        WebElement shadowroot2 = expandRootElement(driver2.findElement(By.xpath("//es-mc-choices[@class='hydrated']")));
        List<WebElement> answers = shadowroot2.findElements(By.tagName("li"));
        WebElement selectA = answers.get(0);

        selectA.click();
        Thread.sleep(2000);
        WebElement submitButton = takeExam.getSubmitButton();

        waitForElementAndClick(submitButton);
        WebElement nextBtn = administerExam.getNextButton();

        nextBtn.click();

        WebElement completeBtn = administerExam.getCompleteBtn();

        waitForElementAndClick(completeBtn);

        WebElement yesBtn = administerExam.getYesButton();

        waitForElementAndClick(yesBtn);

        //nextBtn.click();
    }
}
