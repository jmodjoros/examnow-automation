package com.examsoft.app.Enterprise.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

public class AssessmentPage {
    WebDriver driver;

    By createAssessment = By.id("createAssessment");
    By assessmentTitle = By.xpath("//input[@id='assessmentTitle']");
    By selectExamType = By.id("assessmentType");
    By nextButton = By.xpath("//button[@class='button button-primary right ng-scope']");
    By saveContinue = By.xpath("//button[@id='continueToPosting']");
    By noBlueprint = By.xpath("//button[@class='button button-secondary']");
    By continueButton = By.xpath("//button[@class='button button-primary continue-btn']");
    By slideoutButton = By.xpath("//a[@class='slideout-button']");
    By findQuestions = By.xpath("//button[contains(text(),'Find Questions')]");
    By selectAllQs = By.xpath("//label[@class='grid-cell__select-icons']//icon[@name='plus']//*[@id='Layer_1']");
    By saveButton = By.xpath("//div[@class='columns ng-scope ng-isolate-scope small-4']//button[@class='button-primary right ng-scope'][contains(text(),'Save')]");
    By saveStuButton = By.xpath("//button[@class='button-primary ng-scope']");
    By randomlyGenerate = By.xpath("//label[contains(text(),'Randomly Generate')]");
    By assignStudentsButton = By.xpath("//button[@id='assignStudents']");
    By toastMsg = By.id("hiddenWirisEditor");
    By scheduledOnField = By.xpath("//*[@id='assessmentDownloads']/div[2]/div[2]/form/div/row[3]/div/column/div/date-picker/div/div/input");
    By finalizeButton = By.xpath("//*[@id=\"assessmentPost\"]/row[2]/div/column[2]/div/button[1]");
    By postButton = By.xpath("//*[@id='assessmentPostConfirmationModal']/div/div/div[3]/button[2]");

    public AssessmentPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getPostBtn(){
        return driver.findElement(postButton);
    }
    public WebElement getFinalizeBtn(){
        return driver.findElement(finalizeButton);
    }
    public WebElement getScheduledOnField(){
        return driver.findElement(scheduledOnField);
    }
    public WebElement getToastMsg(){
        return driver.findElement(toastMsg);
    }
    public WebElement getSaveStudent(){
        return driver.findElement(saveStuButton);
    }
    public WebElement getNextButton() {
        return driver.findElement(nextButton);
    }

    public WebElement getAssessmentTitle() {
        return  driver.findElement(assessmentTitle);
    }

    public WebElement getAssignStudentsButton() {
        return driver.findElement(assignStudentsButton);
    }

    public WebElement getContinueButton() {
        return driver.findElement(continueButton);
    }

    public WebElement getCreateAssessment() {
        return driver.findElement(createAssessment);
    }

    public WebElement getFindQuestions() {
        return driver.findElement(findQuestions);
    }

    public WebElement getNoBlueprint() {
        return driver.findElement(noBlueprint);
    }

    public WebElement getRandomlyGenerate() {
        return driver.findElement(randomlyGenerate);
    }

    public WebElement getSaveButton() {
        return driver.findElement(saveButton);
    }

    public WebElement getSaveContinue() {
        return driver.findElement(saveContinue);
    }

    public WebElement getSelectAllQs() {
        return driver.findElement(selectAllQs);
    }

    public WebElement getSelectExamType() {
        return driver.findElement(selectExamType);
    }

    public WebElement getSlideoutButton() {
        return driver.findElement(slideoutButton);
    }

    public By getSaveContinueElement(){
    return saveContinue;}

    public By getSaveStuElement(){
        return saveStuButton;
    }
    }