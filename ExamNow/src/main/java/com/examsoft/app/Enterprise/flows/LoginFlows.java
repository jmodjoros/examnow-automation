package com.examsoft.app.Enterprise.flows;

import com.examsoft.app.Enterprise.base.base;
import com.examsoft.app.Enterprise.pages.AppsPage;
import com.examsoft.app.Enterprise.pages.LoginPage;
import com.examsoft.app.Enterprise.pages.SelectInstitutionPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class LoginFlows extends base {


    public LoginFlows(WebDriver driver)  {
        this.driver = driver;
    }


    public void LocalAdminLogin() throws IOException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        LoginPage l = new LoginPage(driver);

        l.getUsernameField().sendKeys(prop.getProperty("enterpriseAdminUsername"));
        l.getPasswordField().sendKeys(prop.getProperty("enterpriseAdminPassword"));
        l.getLoginBtn().click();
    }

    public void studentExamNowLogin() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        AppsPage a = new AppsPage(driver);
        //SelectInstitutionPage selectInstitutionPage = new SelectInstitutionPage(driver);
        //base b = new base();




        //assign variables for every button in login flow
        WebElement selectInstitutionField = a.getSelectInstitution();
        WebElement continueBtn = a.getContinueBtn();


        //search for and select institution
        Thread.sleep(2000);
        selectInstitutionField.sendKeys(prop.getProperty("EPDB"));
        driver.findElement(By.xpath("//div[@class='item selected']")).click();
        continueBtn.click();

        WebElement usernameField = a.getUsernameField();
        WebElement passwordField = a.getPasswordField();
        WebElement submitBtn = a.getSubmitBtn();

        //enter admin credentials
        usernameField.sendKeys(prop.getProperty("enterpriseStudentUsername"));
        passwordField.sendKeys(prop.getProperty("enterpriseStudentPassword"));
        submitBtn.click();

        WebElement iAgreeChkBx = a.getAgreeChkBx();

        iAgreeChkBx.click();
        driver.findElement(By.className("progressive")).click();
    }
    //login to ExamNow as an admin
    public void adminExamNowLogin() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        AppsPage a = new AppsPage(driver);


        //assign variables for every button in login flow
        WebElement selectInstitutionField = a.getSelectInstitution();
        WebElement continueBtn = a.getContinueBtn();

        Thread.sleep(10000);
        //search for and select institution
        selectInstitutionField.sendKeys(prop.getProperty("EPDB"));
        driver.findElement(By.xpath("//div[@class='item selected']")).click();
        continueBtn.click();

        WebElement usernameField = a.getUsernameField();
        WebElement passwordField = a.getPasswordField();
        WebElement submitBtn = a.getSubmitBtn();

        //enter admin credentials
        usernameField.sendKeys(prop.getProperty("enterpriseAdminUsername"));
        passwordField.sendKeys(prop.getProperty("enterpriseAdminPassword"));
        submitBtn.click();

        WebElement iAgreeChkBx = a.getAgreeChkBx();

        iAgreeChkBx.click();
        driver.findElement(By.className("progressive")).click();
    }
}
