package com.examsoft.app.Enterprise.pages;

import com.examsoft.app.Legacy.base.base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AudienceResponsePage extends base {
    WebDriver driver;

    public AudienceResponsePage(WebDriver driver) {
        this.driver = driver;

    }




    By completeBtn = By.xpath("//button[contains(text(),'Complete')]");
    By prevButton = By.xpath("//button[contains(text(),'Previous')]");
    By nameFilter = By.id("filterTitle");
    By submitButton = By.xpath("/html/body/app-layout/div/main/div/poll-question/div/div/button");
    By nextButton = By.xpath("/html/body/app-layout/div/main/div/admin-poll-main/div[2]/div[2]/button");
    By yesButton = By.xpath("//button[contains(text(),'Yes')]");


    public WebElement getYesButton(){
        return driver.findElement(yesButton);
    }
    public WebElement getNextButton(){
        return driver.findElement(nextButton);
    }
    public WebElement getSubmitButton() {
        return driver.findElement(submitButton);
    }

    public WebElement getPrevButton(){
        return driver.findElement(prevButton);
    }

    public WebElement getCompleteBtn(){
        return driver.findElement(completeBtn);
    }


}
