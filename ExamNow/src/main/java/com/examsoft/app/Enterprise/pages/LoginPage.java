package com.examsoft.app.Enterprise.pages;

import com.examsoft.app.Enterprise.base.base;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends base {
    WebDriver driver;

    By usernameField = By.id("email");
    By passwordField = By.id("password");
    By loginButton = By.xpath("//button[@class='button button-primary ng-scope']");

    public LoginPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getUsernameField(){
        return driver.findElement(usernameField);
    }

    public WebElement getPasswordField(){
        return driver.findElement(passwordField);
    }

    public WebElement getLoginBtn(){
        return driver.findElement(loginButton);
    }
}
