package com.examsoft.app.Legacy.flows;

import com.examsoft.app.Legacy.base.base;
import com.examsoft.app.Legacy.pages.AssessmentPage;
import com.examsoft.app.Legacy.pages.PostAssessmentModal;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Assessments extends base {

    public Assessments(WebDriver driver)  {
        this.driver = driver;

    }
    public void createARExam() throws InterruptedException, IOException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);
        Thread.sleep(1000);

        AssessmentPage a = new AssessmentPage(driver);
        a.getCreateAssessmentBtn().click();
        a.getCreateARExam().click();
        a.getAssessmentName().sendKeys(prop.getProperty("assessmentName"));
        Thread.sleep(300);
        a.getSelectFolderBtn().click();
        a.getChooseFolder().click();
        Thread.sleep(1000);
        a.getAddQuestions().click();
        Thread.sleep(3000);

        a.getChooseQuestionFolder().click();
        Thread.sleep(3000);
        a.getSelectAll().click();
        a.getAddSelectedBtn().click();
        Thread.sleep(1000);

        a.getCloseBtn().click();
        Thread.sleep(1000);

        a.getSaveAssessment().click();
        Thread.sleep(3000);

        a.getPostExamBtn().click();
    }

    public String postARExam() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Automation/src/credentials.properties");
        prop.load(fis);

        SimpleDateFormat todaysDate = new SimpleDateFormat("MM/dd/yyyy");
        PostAssessmentModal p = new PostAssessmentModal(driver);
        Date date = new Date();
        driver.findElement(By.xpath("//input[@class='scombobox-display']")).clear();
        driver.findElement(By.xpath("//input[@class='scombobox-display']")).sendKeys("[000AutoQA] Automation Course");
        p.getScheduledOnField().sendKeys(todaysDate.format(date)+" 11:59 pm");
        p.getPostAssessmentButton().click();
        try {
            WebDriverWait wait = new WebDriverWait(driver, 2);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            System.out.println(alert.getText());
            alert.accept();

        } catch (Exception e) {
            System.out.println("No alert present");        }
        String postingID = driver.findElement(By.xpath("//*[@id='examPostingsTable']/tbody/tr/td[2]")).getText();
        System.out.println(postingID);
        return postingID;

    }
}



