package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {
    WebDriver driver;
    By navAssessmentBtn = By.xpath("//a[@class='navigation assessments']");
    By navExamNowBtn = By.xpath("//a[@class='navigation examnow']");

    public LandingPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getNavAssessmentBtn() {
        return driver.findElement(navAssessmentBtn);
    }

    public WebElement getNavExamNowBtn() {
        return driver.findElement(navExamNowBtn);
    }
}
