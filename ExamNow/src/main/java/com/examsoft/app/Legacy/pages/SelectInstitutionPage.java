package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SelectInstitutionPage {
    WebDriver driver;
    By chooseInstitution = By.xpath("//input[@placeholder='Choose your Institution']");
    By continueButton = By.xpath("//button[@class='progressive']");
    By incompleteType = By.cssSelector("input[type='text']");

    public SelectInstitutionPage(WebDriver driver){
        this.driver = driver;
    }
    public WebElement selectInstitution(){
        return driver.findElement(chooseInstitution);
    }
    public WebElement selectInstitutionInc(){
        return driver.findElement(incompleteType);
    }
    public WebElement getContinueButton(){
        return driver.findElement(continueButton);
    }

}
