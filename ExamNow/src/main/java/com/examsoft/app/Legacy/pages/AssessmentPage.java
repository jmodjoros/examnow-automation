package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AssessmentPage {

    WebDriver driver;

    By createAssessmentBtn = By.id("createAssessment");
    By createARExam = By.xpath(("/html/body/div[20]/div[2]/div/table/tbody/tr/td/div/table/tbody/tr/td/table/tbody/tr[1]/td[3]/div/a"));
    By assessmentName = By.id("examTitle");
    By selectFolderBtn = By.id("selectExamFolderOpen");
    By addQuestions = By.xpath("//a[@class='buttonintable examsAddQuestionsOpen']");
    By saveAssessment = By.xpath("//td[@class='right']//a[@class='button'][contains(text(),'Save')]");
    By chooseFolder = By.xpath("//span[@class='fancytree-title'][contains(text(),'000Auto')]");
    By chooseQuestionFolder = By.xpath("//div[@id='folderTreeDiv']//ul//li//span[@class='td'][contains(text(),'000AutoQA')]");
    By selectAll =  By.id("qSelectCheckAll");
    By addSelectedBtn = By.xpath("//a[contains(text(),'Add Selected to Assessment')]");
    By postExamBtn = By.xpath("//a[@class='button postExamOpen']");
    By closeBtn = By.xpath("//a[@class='keybutton'][contains(text(),'Close')]");

    public AssessmentPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getCloseBtn(){
        return driver.findElement(closeBtn);
    }

    public WebElement getPostExamBtn(){
        return driver.findElement(postExamBtn);
    }

    public WebElement getSelectAll(){
        return driver.findElement(selectAll);
    }

    public WebElement getAddSelectedBtn(){
        return driver.findElement(addSelectedBtn);
    }

    public WebElement getChooseQuestionFolder(){
        return driver.findElement(chooseQuestionFolder);
    }

    public WebElement getAssessmentName() {
        return driver.findElement(assessmentName);
    }

    public WebElement getSelectFolderBtn() {
        return driver.findElement(selectFolderBtn);
    }

    public WebElement getAddQuestions() {
        return driver.findElement(addQuestions);
    }

    public WebElement getSaveAssessment(){
        return driver.findElement(saveAssessment);
    }

    public WebElement getCreateAssessmentBtn() {
        return driver.findElement(createAssessmentBtn);
    }

    public WebElement getCreateARExam() {
        return driver.findElement(createARExam);
    }

    public WebElement getChooseFolder(){
        return driver.findElement(chooseFolder);
    }

}
