package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ExamNowPage {
    WebDriver driver;


    //List<WebElement> startList = driver.findElements(By.xpath("//button[contains(text(),'Start')]"));
    //WebElement startButton = startList.get(startList.size() - 1);
    By startButton = By.xpath("//button[contains(text(),'Start')]");
    By completeBtn = By.xpath("//button[contains(text(),'Complete')]");
    By prevButton = By.xpath("//button[contains(text(),'Previous')]");
    By nameFilter = By.id("filterTitle");
    By bSelection = By.cssSelector("button:nth-child(2)");


    public ExamNowPage(WebDriver driver) {
        this.driver = driver;


    }

    public WebElement getNameFilter(){
        return driver.findElement(nameFilter);
    }
    public WebElement getStartBtn() {
        return driver.findElement(startButton);
    }


}


