package com.examsoft.app.Legacy.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;


public class base {
    public WebDriver driver;
    public WebDriver driver2;




    public WebDriver initializeDriver() throws IOException {


        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Auto/ExamNow/credentials.properties");
        prop.load(fis);
        String browserName = prop.getProperty("browser");


        //chrome
        if(browserName.equals("chrome"))
        {
            WebDriverManager.chromedriver().setup();

            //System.setProperty("webdriver.chrome.driver","/Users/joshrobbins/Documents/Dev Resources/chromedriver");
            driver = new ChromeDriver();
        }
        //firefox
        else if(browserName.equals("firefox"))
        {
            WebDriverManager.chromedriver().setup();

            //System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
            driver = new FirefoxDriver();
        }

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return driver;
    }

    public WebDriver initializeStdDriver() throws IOException {


        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Auto/ExamNow/credentials.properties");
        prop.load(fis);
        String browserName = prop.getProperty("browser");


        //chrome
        if(browserName.equals("chrome"))
        {
            WebDriverManager.chromedriver().setup();

            //System.setProperty("webdriver.chrome.driver","/Users/joshrobbins/Documents/Dev Resources/chromedriver");
            driver2 = new ChromeDriver();
        }
        //firefox
        else if(browserName.equals("firefox"))
        {
            WebDriverManager.chromedriver().setup();

            //System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
            driver2 = new FirefoxDriver();
        }

        driver2.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver2;
    }

    public void switchToTab(WebDriver driver,int a){
        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        //System.out.println(tabs.size());
        driver.switchTo().window(tabs.get(a));
    }

    public void waitForElementAndClick(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void waitForElementAndClickStu(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver2, 30);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public WebElement expandRootElement(WebElement element) {
        WebElement ele = (WebElement) ((JavascriptExecutor) driver2).executeScript("return arguments[0].shadowRoot",element);
        return ele;
    }


}
