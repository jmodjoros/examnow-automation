package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LegacyLoginPage {
    WebDriver driver;

    By adminUsernameField = By.id("userid");
    By adminPasswordField = By.id("password");
    By studentUsernameField = By.id("userid2");
    By studentPasswordField = By.id("password2");
    By adminLoginButton = By.id("emLoginLink");
    By studentLoginButton = By.id("etLoginLink");

    public LegacyLoginPage(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getAdminPasswordField() {
        return driver.findElement(adminPasswordField);
    }

    public WebElement getAdminUsernameField() {
        return driver.findElement(adminUsernameField);
    }

    public WebElement getStudentPasswordField() {
        return driver.findElement(studentPasswordField);
    }

    public WebElement getStudentUsernameField() {
        return driver.findElement(studentUsernameField);
    }

    public WebElement getAdminLoginButton(){
        return driver.findElement(adminLoginButton);
    }
    public WebElement getStudentLoginButton(){
        return driver.findElement(studentLoginButton);
    }
}
