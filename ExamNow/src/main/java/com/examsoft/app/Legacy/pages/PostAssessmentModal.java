package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PostAssessmentModal {
    WebDriver driver;

    By courseDropdown = By.xpath("//select[@name='courseName']");
    By scheduledOnDate = By.id("examDateForDisplay");
    By postAssessment = By.id("viewPostExam");

    public PostAssessmentModal(WebDriver driver){
        this.driver = driver;
    }

    public WebElement getCourseDropdown(){
        return driver.findElement(courseDropdown);
    }
    public WebElement getScheduledOnField(){
        return driver.findElement(scheduledOnDate);
    }
    public WebElement getPostAssessmentButton(){
        return driver.findElement(postAssessment);
    }
}
