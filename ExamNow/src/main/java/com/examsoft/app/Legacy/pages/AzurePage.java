package com.examsoft.app.Legacy.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class AzurePage {


    WebDriver driver;

    By nextButton = By.id("idSIButton9");
    By emailField = By.name("loginfmt");
    By passwordField = By.name("passwd");


    public AzurePage(WebDriver driver) {
        this.driver = driver;

    }
    public WebElement getEmailField() {
        return driver.findElement(emailField);
    }

    public WebElement getNextButton() {
        return driver.findElement(nextButton);
    }

    public WebElement getPasswordField() {
        return driver.findElement(passwordField);
    }






   /* WebElement signInButton1;
    signInButton1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]")));
        signInButton1.click();

    WebElement signInButton2;
    signInButton2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("idSIButton9")));
        signInButton2.click();*/




   /* public void SAMLLogin() throws IOException {
        //selenium code
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("/Users/joshrobbins/IdeaProjects/ExamNow-Automation/src/credentials.properties");

        prop.load(fis);
        //Create Driver object for browser of choice
        driver = initializeDriver();
        driver.get("https://www.google.com");
        WebDriverWait wait = new WebDriverWait(driver,30);

        driver.get("https://apps-qa.examsoft.com/");
        System.out.println("Title of page: "+driver.getTitle());
        System.out.println("Validate that URL is correct: "+driver.getCurrentUrl());
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

        driver.findElement(By.cssSelector("#institution > div > input[type=text]")).sendKeys("QATHREE - Dev");
        driver.findElement(By.xpath("//*[@id='institution']/div/div/div")).click();
        System.out.println("Selected QATHREE");
        driver.findElement(By.className("progressive")).click();

        System.out.println("Launched ExamNow page");
        driver.findElement(By.xpath("/html/body/app-root/main/div/app-home/section[1]/div/es-panel[2]/a/button")).click();
        driver.findElement(By.cssSelector("#institution > div > input[type=text]")).sendKeys("QATHREE - Dev");
        driver.findElement(By.xpath("//*[@id='institution']/div/div/div")).click();//System.out.println(driver.getPageSource());
        System.out.println("Selected QATHREE on ExamNow page");
        driver.findElement(By.className("progressive")).click();

        System.out.println("Launched IDP");
        driver.findElement(By.name("loginfmt")).sendKeys(prop.getProperty("ssoAdminUsername"));
        driver.findElement(By.id("idSIButton9")).click();
        driver.findElement(By.name("passwd")).sendKeys("examsoft1!");
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        //Current pw: ID: currentPassword
        //new pw: ID: newPassword
        //confirm password: ID: confirmNewPassword

        WebElement signInButton1;
        signInButton1 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html[1]/body[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/input[1]")));
        signInButton1.click();

        WebElement signInButton2;
        signInButton2 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("idSIButton9")));
        signInButton2.click();

        driver.findElement(By.cssSelector("div.app-layout-wrapper div.main-inner eula:nth-child(2) es-agreement.hydrated div.es-agreement div.agreement-control:nth-child(2) > label:nth-child(2)")).click();
        driver.findElement(By.className("progressive")).click();

        driver.close();
        //driver.quit();*/
}
