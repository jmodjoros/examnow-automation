package com.examsoft.app.Legacy.flows;

import com.examsoft.app.Legacy.base.base;
import com.examsoft.app.Legacy.pages.AudienceResponsePage;
import com.examsoft.app.Legacy.pages.ExamNowPage;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.util.List;

public class ExamNowFlow extends base {
    public ExamNowFlow(WebDriver driver,WebDriver driver2)  {
        this.driver = driver;
        this.driver2 = driver2;



    }
    public void administerExam(String postingID) throws InterruptedException, IOException {

        ExamNowPage e = new ExamNowPage(driver);
        Thread.sleep(2000);

        WebElement nameFilter = e.getNameFilter();

        Thread.sleep(2000);
        nameFilter.sendKeys(postingID);
        WebElement startBtn = e.getStartBtn();

        waitForElementAndClick(startBtn);

    }
    public void takeExam() throws InterruptedException {
        AudienceResponsePage takeExam = new AudienceResponsePage(driver2);
        AudienceResponsePage administerExam = new AudienceResponsePage(driver);


        WebElement shadowroot1 = expandRootElement(driver2.findElement(By.xpath("//es-mc-choices[@class='hydrated']")));
        List<WebElement> answers = shadowroot1.findElements(By.tagName("li"));
        WebElement selectC = answers.get(2);

        selectC.click();
        Thread.sleep(2000);
        WebElement submitButton = takeExam.getSubmitButton();

        waitForElementAndClick(submitButton);
        WebElement nextBtn = administerExam.getNextButton();

        nextBtn.click();

        WebElement completeBtn = administerExam.getCompleteBtn();

        waitForElementAndClick(completeBtn);

        WebElement yesBtn = administerExam.getYesButton();

        waitForElementAndClick(yesBtn);

        //nextBtn.click();
    }
}
