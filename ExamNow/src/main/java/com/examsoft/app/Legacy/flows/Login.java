package com.examsoft.app.Legacy.flows;


import com.examsoft.app.Legacy.base.base;
import com.examsoft.app.Legacy.pages.AppsPage;
import com.examsoft.app.Legacy.pages.LegacyLoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class Login extends base{

    public Login(WebDriver driver) throws IOException {
        this.driver = driver;

    }

    //login to ExamSoft as an admin
    public void adminLocalLogin() throws IOException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        LegacyLoginPage l = new LegacyLoginPage(driver);

        WebElement adminUsernameField = l.getAdminUsernameField();
        WebElement adminPasswordField = l.getAdminPasswordField();
        WebElement adminLoginButton = l.getAdminLoginButton();

        adminUsernameField.sendKeys(prop.getProperty("localAdminUsername"));
        adminPasswordField.sendKeys(prop.getProperty("localAdminPassword"));
        adminLoginButton.click();

    }

    public void studentLocalLogin() throws IOException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        LegacyLoginPage l = new LegacyLoginPage(driver);


        WebElement studentUsernameField =   l.getStudentUsernameField();
        WebElement studentPasswordField =   l.getStudentPasswordField();
        WebElement studentLoginButton =     l.getStudentLoginButton();


        studentUsernameField.sendKeys(prop.getProperty("localStudentUsername"));
        studentPasswordField.sendKeys(prop.getProperty("localStudentPassword"));
        studentLoginButton.click();

    }
    public void studentExamNowLogin() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        AppsPage a = new AppsPage(driver);
        //base b = new base();




        //assign variables for every button in login flow
        WebElement selectInstitutionField = a.getSelectInstitution();
        WebElement continueBtn = a.getContinueBtn();


        //search for and select institution
        selectInstitutionField.sendKeys(prop.getProperty("localDB"));
        driver.findElement(By.xpath("//div[@class='item selected']")).click();
        continueBtn.click();

        WebElement usernameField = a.getUsernameField();
        WebElement passwordField = a.getPasswordField();
        WebElement submitBtn = a.getSubmitBtn();

        //enter admin credentials
        usernameField.sendKeys(prop.getProperty("localStudentUsername"));
        passwordField.sendKeys(prop.getProperty("localStudentPassword"));
        submitBtn.click();

        WebElement iAgreeChkBx = a.getAgreeChkBx();

        iAgreeChkBx.click();
        driver.findElement(By.className("progressive")).click();
    }
    //login to ExamNow as an admin
    public void adminExamNowLogin() throws IOException, InterruptedException {
        Properties prop = new Properties();
        FileInputStream fis = new FileInputStream("credentials.properties");
        prop.load(fis);

        AppsPage a = new AppsPage(driver);


        ArrayList<String> tabs = new ArrayList<> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        //assign variables for every button in login flow
        WebElement selectInstitutionField = a.getSelectInstitution();
        WebElement continueBtn = a.getContinueBtn();

        Thread.sleep(10000);
        //search for and select institution
        selectInstitutionField.sendKeys(prop.getProperty("localDB"));
        driver.findElement(By.xpath("//div[@class='item selected']")).click();
        continueBtn.click();

        WebElement usernameField = a.getUsernameField();
        WebElement passwordField = a.getPasswordField();
        WebElement submitBtn = a.getSubmitBtn();

        //enter admin credentials
        usernameField.sendKeys(prop.getProperty("localAdminUsername"));
        passwordField.sendKeys(prop.getProperty("localAdminPassword"));
        submitBtn.click();

        WebElement iAgreeChkBx = a.getAgreeChkBx();

        iAgreeChkBx.click();
        driver.findElement(By.className("progressive")).click();
    }

}
